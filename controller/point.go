package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
)

func PointByMatchWeek (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	var idMatchWeek = c.PostForm("id_mw")

	data,err := model.KlasemenMatchWeekIdUser(idUser,idMatchWeek)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Sukses Get Point ",data)

}
