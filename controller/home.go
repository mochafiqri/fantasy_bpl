package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/bandros/framework"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Home (c *gin.Context){
	session := sessions.Default(c)
	v := session.Get(framework.Config("sessionName"))
	var sign= "Sign In"
	if v != nil {
		sign = "Sign Out"
	}
	data,err := model.GetMainLastBeforeMatchWeek()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var playerGK = []map[string]interface{}{}
	var playerDef = []map[string]interface{}{}
	var playerMid = []map[string]interface{}{}
	var playerFwd = []map[string]interface{}{}


	if data != nil {
		playerGK,err = model.PlayerPointByMatchWeekAndPosisition(data["id_mw"].(string),"1")
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
		playerDef,err = model.PlayerPointByMatchWeekAndPosisition(data["id_mw"].(string),"2")
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
		playerMid,err = model.PlayerPointByMatchWeekAndPosisition(data["id_mw"].(string),"3")
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
		playerFwd,err = model.PlayerPointByMatchWeekAndPosisition(data["id_mw"].(string),"4")
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
	}
	css := []string{

		"asset/plugins/datatables/datatables",
	}
	js := []string{
		"asset/plugins/datatables-2/datatables",
		"/asset/plugins/bootstrap-notify/bootstrap-notify",
		"/asset/js/pages/ui/notifications",
		"/asset/js/main/home",
	}
	c.HTML(http.StatusOK,"user/home",gin.H{
		"playerGK" : playerGK,
		"playerDef" : playerDef,
		"playerMid" : playerMid,
		"playerFwd" : playerFwd,
		"title" : "Home",
		"js" : js,
		"css" : css,
		"sign" : sign,
	})
}