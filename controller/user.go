package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/bandros/framework"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func UserLogin(c *gin.Context) {
	var email = c.PostForm("user")
	if email == "" {
		lib.JSON(c, http.StatusInternalServerError, "Username Dan Password Salah", gin.H{})
		return
	}
	var password = c.DefaultPostForm("password", "")
	login, err := model.CekLogin(email, password)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if login == nil {
		lib.JSON(c, http.StatusInternalServerError, "Username Dan Password Salah", gin.H{})
		return
	}
	var result = map[string]interface{}{}

	token, err := lib.GenerateTokenUser(login)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	result["token"] = token
	result["role"] = login["role"]

	session := sessions.Default(c)
	session.Set(framework.Config("sessionName"),token)

	err = session.Save()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c, http.StatusOK, "Anda berhasil login, silahkan tunggu beberapa saat", result)
	return
}


func UserRegister(c *gin.Context){
	var regData model.UserModel
	c.Bind(&regData)

	var number = framework.NumberPhone(regData.NoHp)
	regData.Email = strings.TrimSpace(regData.Email)

	if !lib.ValidationEmail(regData.Email) {
		lib.JSON(c, http.StatusInternalServerError, "E-Mail tidak valid", gin.H{})
		return
	}


	var insert = []string{
		regData.Nama,
		regData.Username,
		number,
		regData.Email,
		framework.Password(regData.Password),
		regData.JK,
		"1", // 1 = User 2 = Admin
	}

	data,err := model.InsertUser(insert)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if data == nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	idUser := data["id_user"].(string)

	var updateUser = map[string]interface{}{
		"nama_team_fantasy" : regData.Username + " FC",
	}

	err = model.UserUpdate(idUser,updateUser)
	if err != nil {
		model.UserDelete(idUser)
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	token,err := model.GenerateTokenIdMember(idUser)
	if err != nil {
		model.UserDelete(idUser)
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var result = map[string]interface{}{}

	result["token"] = token
	result["role"] = "1"
	session := sessions.Default(c)
	session.Set(framework.Config("sessionName"),token)

	err = session.Save()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}


	lib.JSON(c, http.StatusOK, "Berhasil Register, Tunggu beberapa saat", result)
	return
}



