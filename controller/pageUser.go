package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
)

func PageMyTeam (c *gin.Context){

	js := []string{
		"/asset/js/main/my_team",
	}
	c.HTML(http.StatusOK,"user/my_team",gin.H{
		"title" : "Login",
		"js" : js,
	})
}

func PageMyTeam2 (c *gin.Context){
	session := sessions.Default(c)
	v := session.Get(framework.Config("sessionName"))
	var sign= "Sign In"
	if v != nil {
		sign = "Sign Out"
	}
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	dataMW,err := model.GetMainLastMatchWeek()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	dataUser,err := model.UserById(idUser,"id_user,nama_team_fantasy",false)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	css := []string{
		"asset/plugins/datatables/datatables",
	}
	js := []string{
		"asset/plugins/datatables/datatables",
		"/asset/plugins/bootstrap-notify/bootstrap-notify",
		"/asset/js/pages/ui/notifications",
		"/asset/js/main/my_team2",
	}
	c.HTML(http.StatusOK,"user/my_team2",gin.H{
		"user" : dataUser,
		"id_mw" : dataMW["id_mw"],
		"title" : "My Team",
		"js" : js,
		"css" : css,
		"sign" : sign,
	})
}

func PagePoint(c *gin.Context){
	session := sessions.Default(c)
	v := session.Get(framework.Config("sessionName"))
	var sign= "Sign In"
	if v != nil {
		sign = "Sign Out"
	}
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	lastMatchWeek,err := model.GetMainLastMatchWeek()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	dataUser,err := model.UserById(idUser,"id_user,nama_team_fantasy",false)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	dataMatchWeek,err := model.GetMainMatchWeekAktif()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	css := []string{

		"asset/plugins/datatables/datatables",
	}
	js := []string{


		"asset/plugins/datatables/datatables",
		"/asset/plugins/bootstrap-notify/bootstrap-notify",
		"/asset/js/pages/ui/notifications",
		"/asset/js/main/point",
	}
	c.HTML(http.StatusOK,"user/point",gin.H{
		"user" : dataUser,
		"match_week" : dataMatchWeek,
		"id_mw" : lastMatchWeek["id_mw"],
		"title" : "Point",
		"js" : js,
		"css" : css,
		"sign" : sign,

	})
}


func PageKlasemen(c *gin.Context){
	session := sessions.Default(c)
	v := session.Get(framework.Config("sessionName"))
	var sign= "Sign In"
	if v != nil {
		sign = "Sign Out"
	}
	lastMatchWeek,err := model.GetMainLastMatchWeek()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	dataMatchWeek,err := model.GetMainMatchWeekAktif()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	css := []string{

		"asset/plugins/datatables/datatables",
	}
	js := []string{
		"asset/plugins/datatables-2/datatables",
		"/asset/plugins/bootstrap-notify/bootstrap-notify",
		"/asset/js/pages/ui/notifications",
		"/asset/js/main/klasemen",
	}
	c.HTML(http.StatusOK,"user/klasemen",gin.H{
		"match_week" : dataMatchWeek,
		"id_mw" : lastMatchWeek["id_mw"],
		"title" : "Point",
		"js" : js,
		"css" : css,
		"sign" : sign,

	})
}
