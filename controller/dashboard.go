package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Dashboard (c *gin.Context){
	js := []string{
		"/asset/js/dashboard",
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/raphael/raphael.min",
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/chartjs/Chart.bundle",
		"/asset/plugins/flot-charts/jquery.flot",
		"/asset/plugins/flot-charts/jquery.resize",
		"/asset/plugins/flot-charts/jquery.pie",
		"/asset/plugins/flot-charts/jquery.categories",
		"/asset/plugins/flot-charts/jquery.time",
		"/asset/plugins/jquery-sparkline/jquery.sparkline",
	}
	css := []string{
		"/asset/plugins/morrisjs/morris",
	}


	c.HTML(http.StatusOK,"admin/dashboard/index",gin.H{
		"title" : "Dashboard",
		"js" : js,
		"css" : css,})
}
