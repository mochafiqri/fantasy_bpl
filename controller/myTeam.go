package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"fmt"
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

const jerseyDefault  = "https://fantasy.premierleague.com/dist/img/shirts/standard/shirt_0-66.webp"
var maks_player = 11
var maks_player_team = 4
var maks_player_s = 2
var maks_player_a = 2
var maks_player_b = 3
var maks_player_c = 999

func MyTeam (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)
	var idMatchWeek = c.PostForm("id_mw")
	var idPosition = c.PostForm("id_position")
	var jumlahPemain = 1

	if idPosition == "1"{
		jumlahPemain = 1
	}else if (idPosition == "2"){
		jumlahPemain = 4
	}else if (idPosition == "3"){
		jumlahPemain = 4
	}else if (idPosition == "4"){
		jumlahPemain = 2
	}
	result,err := model.TeamByIdUserIdMatchWeek(idUser,idMatchWeek,idPosition)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	for i,v := range result{
		if v["point"].(string) == ""{
			result[i]["point"] = "0"

		}
	}
	if len(result) < jumlahPemain {
		var jumlahKurang = jumlahPemain - len(result)
		for i:=0;i<jumlahKurang;i++{
			result = append(result,map[string]interface{}{
				"nama_player" : "Player",
				"nama_team" : "Team",
				"jersey" : jerseyDefault,
				"point" : "0",
				"nama_position" : "Posisi",
				"no_punggung" : "00",
				"grade" : "00",
			})
		}
	}

	lib.JSON(c,http.StatusOK,"My Team",result)
}


func TransferPlayer (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)
	var playerout = c.PostForm("player_out")
	var playerin = c.PostForm("player_in")

	//get last mw
	dataMW,err := model.GetMainLastMatchWeek()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var idMw = ""
	if dataMW != nil {
		idMw = dataMW["id_mw"].(string)
	}

	dataPlayerIn,err := model.PlayerById(playerin,"p.id_player,p.id_team,p.id_position,p.grade,nama_position")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}


	var db = framework.Database{}
	defer db.Close()
	db.Transaction()
	if playerin == "0" || playerin == ""{
		lib.JSON(c,http.StatusInternalServerError,"Player (IN) wajib diisi",gin.H{})
		return
	}
	var insert = map[string]interface{}{}
	if (playerout != "0" && playerout != "") {
		//Remove
		fmt.Println("Remove")
		err = model.RemovePlayerFantasyTeamDb(db,idUser, playerout, idMw)
		if err != nil {
			db.Rollback()
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
	}

	//Cek Position
	dataJmlPlayerPosition,err  := model.CekPlayerFantasyTeamByPosition(idUser,dataPlayerIn["id_position"].(string),idMw,playerout)
	if err != nil {
		db.Rollback()
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	//Cek jumlah Pemian dalam fantasy

	dataJmlPlayer,err := model.CekPlayerFantasyTeam(idUser,idMw,playerout)
	if err != nil {
		db.Rollback()
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var jmlPlayer = 0
	if dataJmlPlayer != nil {
		jmlPlayer,_ = strconv.Atoi(dataJmlPlayer["jml"].(string))
	}

	if (jmlPlayer >= maks_player){
		db.Rollback()
		lib.JSON(c, http.StatusInternalServerError,"Terjadi kesalahan, Batas 11 pemain yang dapat dipilih ", gin.H{})
		return
	}


	var jmlPlayerPosition = 0
	if dataJmlPlayerPosition != nil {
		jmlPlayerPosition,_ = strconv.Atoi(dataJmlPlayerPosition["jml"].(string))
	}

	var max_position = 0
	if dataPlayerIn["id_position"].(string) == "1"{
		max_position = 1
	}else if (dataPlayerIn["id_position"].(string) == "2"){
		max_position = 4
	}else if (dataPlayerIn["id_position"].(string) == "3"){
		max_position = 4
	}else if (dataPlayerIn["id_position"].(string) == "4"){
		max_position = 2
	}

	if (jmlPlayerPosition >= max_position){
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError,"Terjadi kesalahan, " +
			"Pemain ber posisi "+dataPlayerIn["nama_position"].(string)+" melebih batas" , gin.H{})
		return
	}


	//Cek Pemain Dalam Team
	dataPlayer,err := model.GetPlayerFantasyTeamPlayer(idUser,playerin,idMw,"id_uft")
	if err != nil {
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if dataPlayer != nil {
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError,"Terjadi kesalahan, Pemain sudah dalam team", gin.H{})
		return
	}

	//Cek jumlah Player di 1 team
	dataJmlPlayerTeam,err  := model.CekPlayerFantasyTeamByTeam(idUser,dataPlayerIn["id_team"].(string),idMw,playerout)
	if err != nil {
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var jmlPlayerTeam = 0
	if dataJmlPlayerTeam != nil {
		jmlPlayerTeam,_ = strconv.Atoi(dataJmlPlayerTeam["jml"].(string))
	}
	if (jmlPlayerTeam >= maks_player_team){
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError,"Terjadi kesalahan, Batas 4 pemain yang di pilih setiap team", gin.H{})
		return
	}


	//Cek Jumlah Grade Player
	dataJmlPlayerGrade,err := model.CekPlayerFantasyTeamByGrade(idUser,dataPlayerIn["grade"].(string),idMw,playerout)
	if err != nil {
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var jmlPlayerGrade = 0
	if dataJmlPlayerGrade != nil {
		jmlPlayerGrade,_ = strconv.Atoi(dataJmlPlayerGrade["jml"].(string))
	}

	var max_grade = 0
	if dataPlayerIn["grade"].(string) == "AS" {
		max_grade = maks_player_s
	}else if dataPlayerIn["grade"].(string) == "A" {
		max_grade = maks_player_a
	}else if dataPlayerIn["grade"].(string) == "B" {
		max_grade = maks_player_b
	}else {
		max_grade = maks_player_c
	}

	if (jmlPlayerGrade >= max_grade){
		db.Rollback()

		lib.JSON(c, http.StatusInternalServerError,"Terjadi kesalahan, " +
			"Pemain ber grade "+dataPlayerIn["grade"].(string)+" melebih batas" , gin.H{})
		return
	}


	insert["id_user"] = idUser
	insert["id_player"] = playerin
	insert["id_mw"] = idMw
	_,err =	model.InsertPlayerFantasyTeamDb(db,insert)
	if err != nil {
		db.Rollback()
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	db.Commit()
	lib.JSON(c,http.StatusOK,"Sukses Transfer ",gin.H{})


}
