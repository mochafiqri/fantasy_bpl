package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func KlasemenLimit (c *gin.Context){
	var limit = c.PostForm("limit")
	limitInt,_ := strconv.Atoi(limit)
	if limitInt == 0 {
		limitInt = 10
	}
	data,err := model.KlasemenAllWeekLimit(limitInt)

	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Klasemen All Week",data)
}

func KlasemenByWeekLimit (c *gin.Context){
	idWeek := c.PostForm("id_mw")
	var limit = c.PostForm("limit")
	limitInt,_ := strconv.Atoi(limit)
	if limitInt == 0 {
		limitInt = 10
	}
	data,err := model.KlasemenByWeekLimit(limitInt,idWeek)

	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Klasemen By Week",data)
}

func Klasemen(c *gin.Context){

	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	idMatchWeek := c.PostForm("id_match_week")
	result,err := model.KlasemenMatchWeek(data,idMatchWeek,"")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

