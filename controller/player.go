package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
)

func PlayerListByPosisi(c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)
	idPosition := c.PostForm("id_position")
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	var idSelected = []string{}
	if idUser != "" {
		playerSelected,err := model.GetListPlayerMyTeam(idUser)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		if len(playerSelected) > 0{
			for _,v := range playerSelected{
				idSelected = append(idSelected,v["id_player"].(string))
			}
		}
	}

	if idPosition == ""{
		lib.JSON(c,http.StatusInternalServerError,"ID posisi wajib diisi",gin.H{})
		return
	}

	result,err := model.PlayerByIdPositionNoSelected(data,idPosition,idSelected)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	c.JSON(http.StatusOK,&result)


}

func PlayerAll (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	result,err := model.PlayerAll(data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	c.JSON(http.StatusOK,&result)
}