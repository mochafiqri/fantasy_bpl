package controller

import (
	"fantasy_bpl/lib"
	"fantasy_bpl/model"
	"github.com/bandros/framework"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
)

func LoginPage (c *gin.Context){
	c.HTML(http.StatusOK,"login/index",gin.H{
		"title" : "Login",
	})
}

func Login(c *gin.Context) {
	var email = c.PostForm("user")
	if email == "" {
		lib.JSON(c, http.StatusInternalServerError, "Username Dan Password Salah", gin.H{})
		return
	}
	var password = c.DefaultPostForm("password", "")
	login, err := model.CekLogin(email, password)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if login == nil {
		lib.JSON(c, http.StatusInternalServerError, "Username Dan Password Salah", gin.H{})
		return
	}
	var result = map[string]interface{}{}
	var token = ""
	if login["role"] == "1"{
		token, err = lib.GenerateTokenUser(login)
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}

	}else if login["role"] == "2"{
		token, err = lib.GenerateTokenAdmin(login)
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
	}else{
		lib.JSON(c, http.StatusInternalServerError, "Terjadi Error Hubungi Admin", gin.H{})
		return
	}
	result["token"] = token
	result["role"] = login["role"]
	session := sessions.Default(c)
	session.Set(framework.Config("sessionName"),token)

	err = session.Save()
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c, http.StatusOK, "Anda berhasil login, silahkan tunggu beberapa saat", result)
	return
}


func Logout(c *gin.Context)  {
	session := sessions.Default(c)
	session.Delete(framework.Config("sessionName"))
	session.Save()
	c.Redirect(http.StatusFound,"/login")
}
