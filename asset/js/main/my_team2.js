$(document).ready( function () {
    var idUser = $("#id-user").val();
    var idMw = $("#id-mw").val();
    var tableGK = $('#table-gk').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                d.id_mw = $("#id-mw").val(),
                d.id_position = 1
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player" },
            { data: "no_punggung" },
            { data: "grade" },
            { "defaultContent": "<button class=\"btn btn-outline-danger btn-sm btn-transfer \">\n" +
                    "     <i class=\"fa fa-caret-down \"></i>" +
                    "     </button>"}
        ],
    } );

    var tableDef = $('#table-def').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#id-mw").val(),
                    d.id_position = 2
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player" },
            { data: "no_punggung" },
            { data: "grade" },
            { "defaultContent": "<button class=\"btn btn-outline-danger btn-sm btn-transfer \">\n" +
                    "     <i class=\"fa fa-caret-down \"></i>" +
                    "     </button>"}
        ],
    } );


    var tableMid = $('#table-mid').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#id-mw").val(),
                    d.id_position = 3
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player" },
            { data: "no_punggung" },
            { data: "grade" },
            { "defaultContent": "<button class=\"btn btn-outline-danger btn-sm btn-transfer \">\n" +
                    "     <i class=\"fa fa-caret-down \"></i>" +
                    "     </button>"}
        ],
    } );

    var tableFwd = $('#table-fwd').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#id-mw").val(),
                    d.id_position = 4
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player" },
            { data: "no_punggung" },
            { data: "grade" },
            { "defaultContent": "<button class=\"btn btn-outline-danger btn-sm btn-transfer\">\n" +
                    "     <i class=\"fa fa-caret-down \"></i>" +
                    "     </button>"}
        ],
    } );

    var tableAllPlayer = $('#table-allPlayer').DataTable({
        "pageLength": 17,
        "searching": true,
        "paging":   true,
        "lengthChange": false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "columnDefs": [
            {
                targets: 1,
                className: 'dt-body-left'
            }
        ],
        "ajax": {
            "url": "user/api/player/listAll",
            "data" :   {
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_player+")"
                },className: "text-left"},
            { data: "no_punggung" },
            { data: "grade" },
            { "defaultContent": "<button class=\"btn btn-outline-success btn-sm btn-proses-in\">\n" +
                    "     <i class=\"fa fa-caret-up \"></i>" +
                    "     </button>"}
        ],
    } );


    var tableTrf = $('#table-trf').DataTable({})

    $('#table-gk tbody').on( 'click', '.btn-transfer', function (e) {
        var id_out = tableGK.row($(this).parents('tr')).data().id_player;
        $("#player_out").val(id_out);
        $("#transferModal").modal('show');
        loadTableTrf(1,1)
    });

    $('#table-def tbody').on( 'click', '.btn-transfer', function (e) {
        var id_out = tableDef.row($(this).parents('tr')).data().id_player;
        $("#player_out").val(id_out);
        $("#transferModal").modal('show');
        loadTableTrf(1,2)
    });

    $('#table-mid tbody').on( 'click', '.btn-transfer', function (e) {
        var id_out = tableMid.row($(this).parents('tr')).data().id_player;
        $("#player_out").val(id_out);
        $("#transferModal").modal('show');
        loadTableTrf(1,3)
    });

    $('#table-fwd tbody').on( 'click', '.btn-transfer', function (e) {
        var id_out = tableFwd.row($(this).parents('tr')).data().id_player;
        $("#player_out").val(id_out);
        $("#transferModal").modal('show');
        loadTableTrf(1,4)
    });
    function loadTableTrf(idUser,idPosisition) {
        tableTrf.destroy();
        tableTrf = $('#table-trf').DataTable({
            "pageLength": 50,
            "searching": true,
            "paging":   true,
            "ordering": false,
            "info":     false,
            "responsive": true,
            "serverSide": true,
            "ajax": {
                "url": "user/api/player/listByPosisi",
                "data" :   {
                    "id_user" : idUser,
                    "id_position" : idPosisition,
                },
                "type": "POST"
            },
            columns: [
                { data: "no_punggung" },
                { data: "jersey",
                    render: function (data, type, row, meta) {
                        return "<img src='"+data+"'" +
                            "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                    }},
                { data: "nama_player" },
                { data: "nama_position" },
                { data: "grade" },
                { "defaultContent": "<button class=\"btn btn-outline-success btn-sm btn-transfer-proses\">\n" +
                        "     <i class=\"fa fa-caret-up \"></i>" +
                        "     </button>"}
            ],
        } );

    }
    $('#table-trf tbody').on( 'click', '.btn-transfer-proses', function (e) {
        e.preventDefault()
        var id_in = tableTrf.row($(this).parents('tr')).data().id_player;
        var id_out = $("#player_out").val();

        $.ajax({
            url : "user/api/player/transfer",
            data : {
                "player_out" : id_out,
                "player_in" : id_in,
                "id_user" : idUser,
            },
            type : "POST",
            success : function (r) {
                $("#transferModal").modal('hide');
                if (r.code == 200) {
                    showNotification("alert-success",r.msg,
                        "bottom","center","","",);
                    reloadAll();
                }else {
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);
                }
            },
            error : function (r) {
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });

    });


    function reloadAll() {
        tableFwd.ajax.reload();
        tableGK.ajax.reload();
        tableDef.ajax.reload();
        tableMid.ajax.reload();
    }



});



