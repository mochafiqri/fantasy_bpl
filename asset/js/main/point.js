
$(document).ready( function () {
    var idUser = $("#id-user").val();
    var idMw = $("#id-mw").val();
    GetPoint(idMw)

    function GetPoint(id_matchweek){
        $.ajax({
            url : "user/api/match_week/point",
            data : {
                "id_mw": id_matchweek,
            },
            type : "POST",
            success : function (r) {
                $("#transferModal").modal('hide');
                if (r.code == 200) {
                  $(".total_point").html(r.data.point);
                  $(".ranking_klasemen").html(r.data.rank);
                }else {
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);
                }
            },
            error : function (r) {
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
         })
    }
    var tableGK = $('#table-gk').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#select-mw").val(),
                    d.id_position = 1
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player",className: "text-left"},
            { data: "no_punggung" },
            { data: "grade" },
            { data: "point" },

        ],
    } );

    var tableDef = $('#table-def').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#select-mw").val(),
                    d.id_position = 2
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player",className: "text-left" },
            { data: "no_punggung" },
            { data: "grade" },
            { data: "point" },

        ],
    } );


    var tableMid = $('#table-mid').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#select-mw").val(),
                    d.id_position = 3
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player",className: "text-left" },
            { data: "no_punggung" },
            { data: "grade" },
            { data: "point" },

        ],
    } );

    var tableFwd = $('#table-fwd').DataTable({
        "searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        "ajax": {
            "url": "user/api/my-team",
            "data" :   function (d) {
                d.id_user = idUser,
                    d.id_mw = $("#select-mw").val(),
                    d.id_position = 4
            },
            "type": "POST"
        },
        columns: [
            { data: "nama_position" },
            { data: "jersey",
                render: function (data, type, row, meta) {
                    return "<img src='"+data+"'" +
                        "style='width: 25px; height: 35px;'> (" + row.nama_team+")"
                },className: "text-left"},
            { data: "nama_player",className: "text-left" },
            { data: "no_punggung" },
            { data: "grade" },
            { data: "point" },

        ],
    } );

    var tableKlasemenLastWeek = $('#table-klasemen-thistWeek').DataTable({
        "searching": false,
        "paging":   false,
        "lengthChange": false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": false,

        "ajax": {
            "url": "user/api/klasemen-limit/byWeek",
            "data" :   function (d) {
                d.limit = 5,
                d.id_mw = $("#select-mw").val()
            },
            "type": "POST"
        },
        columns: [
            { data: "rank" },
            { data: "nama_team_fantasy",
                render: function (data, type, row, meta) {
                    return "<ul style='list-style-type:none;'>" +
                        "<li class='text-capitalize font-weight-bold'>"+data+"</li>"+
                        "<li> "+row.username+"</li>"
                        "</ul>"
                },className: "text-left"},
            { data: "point" },

        ],
    } );

    var tableKlasemen = $('#table-klasemen').DataTable({
        "searching": false,
        "paging":   false,
        "lengthChange": false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": false,

        "ajax": {
            "url": "user/api/klasemen-limit",
            "data" :   {
                "limit" : 5,
            },
            "type": "POST"
        },
        columns: [
            { data: "rank" },
            { data: "nama_team_fantasy",
                render: function (data, type, row, meta) {
                    return "<ul style='list-style-type:none;'>" +
                        "<li class='text-capitalize font-weight-bold'>"+data+"</li>"+
                        "<li> "+row.username+"</li>"
                    "</ul>"
                },className: "text-left"},
            { data: "point" },

        ],
    } );

    var tableTrf = $('#table-trf').DataTable({})


    function reloadAll() {
        tableFwd.ajax.reload();
        tableGK.ajax.reload();
        tableDef.ajax.reload();
        tableMid.ajax.reload();
        tableKlasemenLastWeek.ajax.reload()

    }
    $("#select-mw").change(function (e) {
        e.preventDefault()
        reloadAll()
        GetPoint( $("#select-mw").val())
    })

});



