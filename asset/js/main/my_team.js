tableGK();
tableDef();
tableMid();
tableFwd();
function tableGK(){
    $.ajax({
        url : "user/api/my-team",
        data : {
            "id_user" : 1,
            "id_mw" : 1,
            "id_position" : 1,
        },
        type : "POST",
        success : function (r) {
            console.log(r)
            if( r.code = 200 ){
                $("#table-gk tbody").html("")
                var value = ""
                $.each(r.data, function(i, data) {
                    nomor = i+1
                    value =
                        "<tr>" +
                        "<td>" + nomor + "</td>" +
                        "<td>" + "<img src='"+data.jersey+"'" +
                        "style='width: 25px; height: 35px;'>" + " " +data.nama_team +
                        "</td>" +
                        "<td>" + data.nama_player + "</td>" +
                        "<td> " + data.nama_position + " </td>" +
                        "<td>" + data.point + "</td>" +
                        "<td> <button class=\"btn btn-outline-danger btn-sm \">\n" +
                        "     <i class=\"fa fa-caret-down \"></i>" +
                        "     </button></td>" +
                        "</tr>";
                    $("#table-gk tbody").append(value)

                });

            }
        },
        error : function (r) {

        }
    });

}

function tableDef(){
    $.ajax({
        url : "user/api/my-team",
        data : {
            "id_user" : 1,
            "id_mw" : 1,
            "id_position" : 2,
        },
        type : "POST",
        success : function (r) {
            console.log(r)
            if( r.code = 200 ){
                $("#table-def tbody").html("")
                var value = ""
                $.each(r.data, function(i, data) {
                    nomor = i+1
                    value =
                        nomor = i+1
                    value =
                        "<tr>" +
                        "<td>" + nomor + "</td>" +
                        "<td>" + "<img src='"+data.jersey+"'" +
                        "style='width: 25px; height: 35px;'>" + " " +data.nama_team +
                        "</td>" +
                        "<td>" + data.nama_player + "</td>" +
                        "<td> " + data.nama_position + " </td>" +
                        "<td>" + data.point + "</td>" +
                        "<td> <button class=\"btn btn-outline-danger btn-sm \">\n" +
                        "     <i class=\"fa fa-caret-down \"></i>" +
                        "     </button></td>" +
                        "</tr>";
                    $("#table-def tbody").append(value)
                });

            }
        },
        error : function (r) {

        }
    });

}

function tableMid(){
    $.ajax({
        url : "user/api/my-team",
        data : {
            "id_user" : 1,
            "id_mw" : 1,
            "id_position" : 3,
        },
        type : "POST",
        success : function (r) {
            console.log(r)
            if( r.code = 200 ){
                $("#table-mid tbody").html("")
                var value = ""
                $.each(r.data, function(i, data) {
                    nomor = i+1
                    value =
                        "<tr>" +
                        "<td>" + nomor + "</td>" +
                        "<td>" + "<img src='"+data.jersey+"'" +
                        "style='width: 25px; height: 35px;'>" + " " +data.nama_team +
                        "</td>" +
                        "<td>" + data.nama_player + "</td>" +
                        "<td> " + data.nama_position + " </td>" +
                        "<td>" + data.point + "</td>" +
                        "<td> <button class=\"btn btn-outline-danger btn-sm \">\n" +
                        "     <i class=\"fa fa-caret-down \"></i>" +
                        "     </button></td>" +
                        "</tr>";
                    $("#table-mid tbody").append(value)
                });

            }
        },
        error : function (r) {

        }
    });

}

function tableFwd(){
    $.ajax({
        url : "user/api/my-team",
        data : {
            "id_user" : 1,
            "id_mw" : 1,
            "id_position" : 4,
        },
        type : "POST",
        success : function (r) {
            console.log(r)
            if( r.code = 200 ){
                $("#table-fwd tbody").html("")
                var value = ""
                $.each(r.data, function(i, data) {
                    nomor = i+1
                    value =
                        "<tr>" +
                        "<td>" + nomor + "</td>" +
                        "<td>" + "<img src='"+data.jersey+"'" +
                        "style='width: 25px; height: 35px;'>" + " " +data.nama_team +
                        "</td>" +
                        "<td>" + data.nama_player + "</td>" +
                        "<td> " + data.nama_position + " </td>" +
                        "<td>" + data.point + "</td>" +
                        "<td> <button class=\"btn btn-outline-danger btn-sm \">\n" +
                        "     <i class=\"fa fa-caret-down \"></i>" +
                        "     </button></td>" +
                        "</tr>";
                    $("#table-fwd tbody").append(value)
                });

            }
        },
        error : function (r) {

        }
    });

}
