var idMw = $("#select-mw").val()

var tableKlasemen = $('#table-klasemen').DataTable({
        "searching": false,
        "paging":   true,
        "lengthChange": false,
        "ordering": false,
        "info":     false,
        "responsive": false,
        "serverSide": true,
        processing: true,
     "ajax": {
            "url": "user/api/klasemen",
            "data" :   function (d) {
                d.limit = "50",
                d.id_match_week = $("#select-mw").val()
            },
            "type": "POST"
        },
        columns: [
            { data: "rank" },
            { data: "nama_team_fantasy",
                render: function (data, type, row, meta) {
                    return "<ul style='list-style-type:none;'>" +
                        "<li class='text-capitalize font-weight-bold'>"+data+"</li>"+
                        "<li> "+row.username+"</li>"
                    "</ul>"
                },className: "text-left"},
            { data: "point" },

        ],
});


$("#select-mw").change(function (e) {
    e.preventDefault()

    idMw =  $("#select-mw").val()
    tableKlasemen.ajax.reload();
})

