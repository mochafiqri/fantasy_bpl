$(".card-signup").hide();

$(".signup_text").click(function (e) {
    e.preventDefault();
    $(".card-login").hide();
    $(".card-signup").show();
})

$(".signin_text").click(function (e) {
    e.preventDefault();
    $(".card-login").show();
    $(".card-signup").hide();
})


$("#frm").submit(function (e) {
    e.preventDefault();

    var url = $(this).attr("action");
    var method = $(this).attr("method");
    var data = $(this).serialize();
 
    var loading = '<i class="fa fa-spin fa-spinner"></i> Proses...';
    var $btn = $(this).find(".btn-submit");
    var btn = $btn.html();
    $btn.html(loading);
    $btn.attr("disabled",true);
    $.ajax({
        url : url,
        data : data,
        type : method,
        success : function (r) {
            if (r.code == 200){
                $(this).trigger("reset");
                showNotification("alert-success","Suskses Login. Tunggu Beberapa Saat",
                    "bottom","center","","",);
                    alert(r.data.role);
                    console.log(r.data);
                    if (r.data.role == "1"){
                        setTimeout(function () {
                            document.location="/my-team";
                            $btn.html(btn);
                            $btn.attr("disabled",false);
                        },2000);
                        return
                    }else if (r.data.role == "2"){
                        setTimeout(function () {
                            document.location="/fantasyadmin";
                            $btn.html(btn);
                            $btn.attr("disabled",false);
                        },2000);
                    }

            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
                $btn.html(btn);
                $btn.attr("disabled",false);
            }
        },
        error : function (r) {
            toastr.error(r.responseJSON.msg);
            $btn.html(btn);
            $btn.attr("disabled",false);
        }
    })
});


$("#frmRegister").submit(function (e) {
    e.preventDefault();

    var url = $(this).attr("action");
    var method = $(this).attr("method");
    var data = $(this).serialize();

    var loading = '<i class="fa fa-spin fa-spinner"></i> Proses...';
    var $btn = $(this).find(".btn-submit");
    var btn = $btn.html();
    $btn.html(loading);
    $btn.attr("disabled",true);
    $.ajax({
        url : url,
        data : data,
        type : method,
        success : function (r) {
            if (r.code == 200){
                $(this).trigger("reset");
                showNotification("alert-success","Suskses Register. Tunggu Beberapa Saat",
                    "bottom","center","","",);
                alert(r.data.role);
                console.log(r.data);
                if (r.data.role == "1"){
                    setTimeout(function () {
                        document.location="/my-team";
                        $btn.html(btn);
                        $btn.attr("disabled",false);
                    },2000);
                    return
                }else if (r.data.role == "2"){
                    setTimeout(function () {
                        document.location="/fantasyadmin";
                        $btn.html(btn);
                        $btn.attr("disabled",false);
                    },2000);
                }

            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
                $btn.html(btn);
                $btn.attr("disabled",false);
            }
        },
        error : function (r) {
            toastr.error(r.responseJSON.msg);
            $btn.html(btn);
            $btn.attr("disabled",false);
        }
    })
});
