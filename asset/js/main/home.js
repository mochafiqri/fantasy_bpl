


var tableKlasemen = $('#table-klasemen').DataTable({
    "searching": false,
    "paging":   false,
    "lengthChange": false,
    "ordering": false,
    "info":     false,
    "responsive": false,
    "serverSide": false,

    "ajax": {
        "url": "user/api/klasemen-limit",
        "data" :   {
            "limit" : 5,
        },
        "type": "POST"
    },
    columns: [
        { data: "rank" },
        { data: "nama_team_fantasy",
            render: function (data, type, row, meta) {
                return "<ul style='list-style-type:none;'>" +
                    "<li class='text-capitalize font-weight-bold'>"+data+"</li>"+
                    "<li> "+row.username+"</li>"
                "</ul>"
            },className: "text-left"},
        { data: "point" },

    ],
} );
