package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func KlasemenMatchWeek(data DatatablesSource, idMatchWeek,idUser string) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"u.nama_team_fantasy",
		"u.nama",
	}

	var db = framework.Database{}
	defer db.Close()

	if (idMatchWeek != ""){
		db.Where("pmw.id_mw",idMatchWeek)
	}

	db.Select("count(u.id_user) num").
	From("user u").
		Join("user_fantasy_team uft","uft.id_user = u.id_user","").
		Join("player_match_week pmw","pmw.id_player = uft.id_player","")
	db.GroupBy("u.id_user")
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	var rt = 0
	var rf = 0
	if count != nil {
		rt, _ = strconv.Atoi(count["num"].(string))
	}
	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	if count != nil {
		rf, _ = strconv.Atoi(count["num"].(string))
	}

	result.RecordsFiltered = rf

	db.Select("u.id_user, u.username, u.nama_team_fantasy, SUM(pmw.point) point")
	//Aktif
	db.Limit(data.Length, data.Start)
	db.OrderBy("point","desc")
	dt, err := db.Result()
	if err != nil {
		return nil, err
	}
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		var rank = data.Start+1
		for i,_ := range dt {
			if i == 0 {
				dt[i]["rank"] = rank
			}else{
				if dt[i]["point"] == dt[i-1]["point"]{
					dt[i]["rank"] = rank
				}else{
					rank = rank + 1
					dt[i]["rank"] = rank
				}
			}
		}
		if (idUser != ""){

			dataUser,err := UserById(idUser,"username,nama_team_fantasy",false)
			if err != nil {
				return nil, err
			}
			dataMyTeam,err := KlasemenMatchWeekIdUser(idUser,idMatchWeek)
			if err != nil {
				return nil, err
			}
			dt = append(dt,map[string]interface{}{
				"rank" :dataMyTeam["rank"],
				"point" :dataMyTeam["point"],
				"username" :dataUser["username"],
				"nama_team_fantasy" :dataUser["nama_team_fantasy"],

			})
		}


		result.Data = dt

	}

	return &result, nil
}

func KlasemenAllWeekLimit (limit int) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("u.id_user, u.username, u.nama_team_fantasy, SUM(pmw.point) point").
		From("user u").
		Join("user_fantasy_team uft","uft.id_user = u.id_user","").
		Join("player_match_week pmw","pmw.id_player = uft.id_player","")
	db.GroupBy("u.id_user").
		OrderBy("point","desc")
	db.Limit(limit,0)
	data,err :=  db.Result()

	if err != nil {
		return nil,err
	}
	var rank = 1
	for i,_ := range data {
		if i == 0 {
			data[i]["rank"] = rank
		}else{
			if data[i]["point"] == data[i-1]["point"]{
				data[i]["rank"] = rank
			}else{
				rank = rank + 1
				data[i]["rank"] = rank
			}
		}
	}
	return data,err
}
func KlasemenByWeekLimit (limit int,idMw string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("u.id_user, u.username, u.nama_team_fantasy, SUM(pmw.point) point").
		From("user u").
		Join("user_fantasy_team uft","uft.id_user = u.id_user","").
		Join("player_match_week pmw","pmw.id_player = uft.id_player","")
	db.Where("pmw.id_mw",idMw)
	db.GroupBy("u.id_user").
		OrderBy("point","desc")
	db.Limit(limit,0)
	data,err :=  db.Result()
	//fmt.Println(db.QueryView())
	if err != nil {
		return nil,err
	}
	var rank = 1
	for i,_ := range data {
		if i == 0 {
			data[i]["rank"] = rank
		}else{
			if data[i]["point"] == data[i-1]["point"]{
				data[i]["rank"] = rank
			}else{
				rank = rank + 1
				data[i]["rank"] = rank
			}
		}
	}
	return data,err
}

func KlasemenMatchWeekIdUser (idUser,idMatchWeek string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("u.id_user, u.username, u.nama_team_fantasy, SUM(pmw.point) point").
		From("user u").
		Join("user_fantasy_team uft","uft.id_user = u.id_user","").
		Join("player_match_week pmw","pmw.id_player = uft.id_player","")
	db.Where("uft.id_mw",idMatchWeek).
		Where("pmw.id_mw",idMatchWeek)
	db.GroupBy("u.id_user").
		OrderBy("point","desc")
	data,err :=  db.Result()
	if err != nil {
		return nil,err
	}
	var rank = 0
	var point float64
	for i,v := range data {
		if i == 0 {
			rank = rank + 1
			data[i]["rank"] = rank
		}else{
			if data[i]["point"] == data[i-1]["point"]{
				data[i]["rank"] = rank
			}else{
				rank = rank + 1
				data[i]["rank"] = rank
			}
		}
		if v["id_user"].(string) == idUser{
			if (v["point"].(string) != ""){
				point,_ = strconv.ParseFloat(v["point"].(string), 64)
			}else{
				point = 0
			}
			break
		}
	}
	var result = map[string]interface{}{
		"rank" : rank,
		"point" : point,
	}
	return result,err
}


