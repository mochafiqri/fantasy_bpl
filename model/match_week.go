package model

import "github.com/bandros/framework"

func GetMainLastMatchWeek() (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id_mw").
		From("match_week").
		Where("status",1)


	db.OrderBy("id_mw","desc")
	return db.Row()
}

func GetMainLastBeforeMatchWeek() (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id_mw").
		From("match_week").
		Where("status",1)


	db.OrderBy("id_mw","desc")
	db.Limit(1,1)
	data,err :=  db.Result()
	if err != nil {
		return nil,err
	}
	if len(data)== 0 {
		return nil,err
	}else{
		return data[0],err
	}
}

func GetMainMatchWeekAktif() ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("mw.id_mw,mw.nama_mw,lf.nama_liga").
		From("match_week mw").
		Join("liga_fantasy lf","mw.id_lf=lf.id_lf","").
		Where("lf.status",1).
		Where("mw.status",1)
	db.OrderBy("mw.id_mw","desc")
	return db.Result()
}
