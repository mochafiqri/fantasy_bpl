package model

type DatatablesResult struct {
	Draw            uint8                    `json:"draw"`
	RecordsTotal    int                      `json:"recordsTotal"`
	RecordsFiltered int                      `json:"recordsFiltered"`
	Data            []map[string]interface{} `json:"data"`
}
type DatatablesSource struct {
	Draw    uint8  `form:"draw"`
	OrdCol  uint8  `form:"order[0][column]"`
	OrdDir  string `form:"order[0][dir]"`
	Start   int    `form:"start"`
	Length  int    `form:"length"`
	Keyword string `form:"search[value]"`
}

type DataSource struct {
	IdMember string `form:"id_member"`
	Page     int    `form:"page"`
	Length   int    `form:"length"`
	Search   string `form:"search"`
	StarDate string `form:"start_date"`
	EndDate  string `form:"end_date"`
	Sort     string `json:"sort"`
	Os       string
}

type DataResult struct {
	TotalData int                      `json:"total_data"`
	Length    int                      `json:"length"`
	NumPage   int                      `json:"num_page"`
	Data      []map[string]interface{} `json:"data"`
	DataPlus  map[string]interface{}   `json:"dataPlus"`
}

type DataSourceReport struct {
	Page      int    `form:"page"`
	Length    int    `form:"length"`
	StartDate string `form:"start_date"`
	EndDate   string `form:"end_date"`
	BrandType int    `form:"brand_type"`
	Brand     string `form:"brand"`
}

type DataResultReport struct {
	TotalData int                      `json:"total_data"`
	Length    int                      `json:"length"`
	NumPage   int                      `json:"num_page"`
	Data      []map[string]interface{} `json:"data"`
	Add       map[string]interface{}   `json:"add"`
}
