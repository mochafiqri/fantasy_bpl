package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func PlayerAll(data DatatablesSource) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"p.no_punggung",
		"t.nama_team",
		"p.nama_player",
	}

	var db = framework.Database{}
	defer db.Close()

	db.Select("count(p.id_player) num")
	db.From("player p").
		Join("team t","p.id_team = t.id_team","").
		Join("position po","p.id_position = po.id_position","").
		Where("p.status",1)
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("p.id_player,p.nama_player,t.jersey,po.nama_position, p.no_punggung,p.grade").From("player p")

	//Aktif
	db.Limit(data.Length, data.Start)
	db.OrderBy("p.grade,p.nama_player","ASC")
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}

	return &result, nil
}

func PlayerByIdPositionNoSelected (data DatatablesSource, idPosition string, idSelected []string) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"p.no_punggung",
		"t.nama_team",
		"p.nama_player",
	}


	var db = framework.Database{}
	defer db.Close()

	db.Select("count(p.id_player) num")
	db.From("player p").
		Join("team t","p.id_team = t.id_team","").
		Join("position po","p.id_position = po.id_position","").
		Where("p.id_position",idPosition).
		WhereNotIn("p.id_player",idSelected).
		Where("p.status",1)
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("p.id_player,p.nama_player,t.nama_team,t.jersey,po.nama_position, p.no_punggung,p.grade").From("player p")

	 //Aktif
	db.Limit(data.Length, data.Start)
	db.OrderBy("p.grade","ASC")
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}

	return &result, nil
}

func PlayerById (idPlayer,sel string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select(sel).
		From("player p").
		Join("position pos","p.id_position=pos.id_position","")
	db.Where("p.id_player",idPlayer)
	return db.Row()
}

func PlayerPointByMatchWeekAndPosisition (idMW,idPosition string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("p.nama_player,p.grade,t.jersey,t.nama_team,pmw.point").
		From("player_match_week pmw").
		Join("player p","pmw.id_player = p.id_player","").
		Join("team t","p.id_team = t.id_team","")
	db.Where("pmw.id_mw",idMW).
		Where("p.status",1).
		Where("p.id_position",idPosition)
	db.OrderBy("pmw.point","DESC")
	db.Limit(5,0)
	data,err :=  db.Result()
	if err != nil {
		return nil,err
	}
	var rank = 1
	for i,_ := range data {
		if i == 0 {
			data[i]["rank"] = rank
		}else{
			if data[i]["point"] == data[i-1]["point"]{
				data[i]["rank"] = rank
			}else{
				rank = rank + 1
				data[i]["rank"] = rank
			}
		}
	}
	return data,err
}