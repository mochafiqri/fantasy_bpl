package model

import (
	"github.com/bandros/framework"
)

func TeamByIdUserIdMatchWeek (idUser,idWeek,idPosition string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("p.id_player,p.nama_player,t.nama_team,t.jersey,pmw.point,po.nama_position, p.no_punggung,p.grade").
		From("user_fantasy_team uft").
		Join("player p","uft.id_player = p.id_player","").
		Join("position po","p.id_position = po.id_position","").
		Join("team t","p.id_team = t.id_team","").
		Join("player_match_week pmw","uft.id_player = pmw.id_player and pmw.id_mw = "+idWeek,"LEFT")
	db.Where("p.id_position",idPosition).
		Where("uft.id_user",idUser).
		Where("uft.id_mw",idWeek)

	data,err := db.Result()
	return data,err

}

func GetListPlayerMyTeam(idUser string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id_player").From("user_fantasy_team uft").
		Where("id_user",idUser)
	return db.Result()
}



func GetPlayerFantasyTeamPlayer(idUser, idPlayer, idMw,sel string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select(sel)
	db.From("user_fantasy_team").
		Where("id_user",idUser).
		Where("id_player",idPlayer).
		Where("id_mw",idMw)
	return db.Row()
}

func CekPlayerFantasyTeam(idUser, idMw string,player_out string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("count(uft.id_player) jml")
	db.From("user_fantasy_team uft").
		Join("player p ","uft.id_player = p.id_player","").
		Where("uft.id_user",idUser).
		Where("uft.id_mw",idMw)

	if player_out != ""{
		db.Where("p.id_player !=",player_out)
	}
	data,err := db.Row()
	return data,err
}

func CekPlayerFantasyTeamByPosition(idUser, idPosition, idMw string,player_out string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("count(uft.id_player) jml")
	db.From("user_fantasy_team uft").
		Join("player p ","uft.id_player = p.id_player","").
		Where("uft.id_user",idUser).
		Where("uft.id_mw",idMw).
		Where("p.id_position",idPosition)

	if player_out != ""{
		db.Where("p.id_player !=",player_out)
	}
	data,err := db.Row()
	return data,err
}

func CekPlayerFantasyTeamByTeam(idUser, idTeam, idMw,player_out string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("count(p.id_player) jml")
	db.From("user_fantasy_team uft").
		Join("player p ","uft.id_player = p.id_player","").
		Where("uft.id_user",idUser).
		Where("uft.id_mw",idMw).
		Where("p.id_team",idTeam)

	if player_out != ""{
		db.Where("p.id_player !=",player_out)
	}
	data,err := db.Row()
	return data,err
}

func CekPlayerFantasyTeamByGrade(idUser, grade, idMw,player_out string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("count(uft.id_player) jml")
	db.From("user_fantasy_team uft").
		Join("player p ","uft.id_player = p.id_player","").
		Where("uft.id_user",idUser).
		Where("uft.id_mw",idMw).
		Where("p.grade",grade)

	if player_out != ""{
		db.Where("p.id_player !=",player_out)
	}
	data,err := db.Row()
	return data,err
}

func RemovePlayerFantasyTeamDb(db framework.Database,idUser, idPlayer, idMw string) error{

	db.From("user_fantasy_team").
		Where("id_user",idUser).
		Where("id_player",idPlayer).
		Where("id_mw",idMw)
	return db.Delete()
}

func InsertPlayerFantasyTeamDb (db framework.Database,insert map[string]interface{}) (interface{},error) {
	return db.From("user_fantasy_team").
		Insert(insert)
}

