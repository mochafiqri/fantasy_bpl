package model

import (
	"fantasy_bpl/lib"
	"github.com/bandros/framework"
)

type UserModel struct {
	Nama     string `form:"nama"`
	Username     string `form:"username"`
	Email     string `form:"email"`
	NoHp    string `form:"no_hp"`
	Password string `form:"password"`
	JK string `form:"jenis_kelamin"`
}


func InsertUser (user []string) (map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Call("insert_user",user)
	return db.Row()
}

func CekLogin (email,password string) (map[string]interface{},error){
	db := framework.Database{}
	defer db.Close()
	db.Select("id_user,email,username,role")
	db.From("user")
	db.StartGroup("AND")
	db.StartGroup("OR")
	db.Where("email", email)
	db.WhereOr("username", email)
	db.EndGroup()
	db.Where("password", framework.Password(password))
	db.EndGroup()
	data, err := db.Row()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func GenerateTokenIdMember (idMember string) (string,error){
	db := framework.Database{}
	defer db.Close()
	db.Select("id_user,email,username,role")
	db.From("user")
	db.Where("id_user", idMember)
	data, err := db.Row()
	if err != nil {
		return "", err
	}
	token,err := lib.GenerateTokenUser(data)
	if err != nil {
		return "", err
	}
	return token, nil
}


func UserUpdate(id string, update map[string]interface{}) error {
	var db =framework.Database{}
	defer db.Close()

	db.From("user").
		Where("id_user",id)
	return db.Update(update)

}

func UserDelete(id string) error{
	var db =framework.Database{}
	defer db.Close()

	db.From("user").
		Where("id_user",id)
	return db.Delete()
}

func UserById(id,sel string,isAdmin bool) (map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select(sel).
		From("user").
		Where("id_user",id)
		if (isAdmin){
			db.Where("role",2)
		}else{
			db.Where("role",1)
		}
		return db.Row()
}
//func CekLoginUser (email,password string) (map[string]interface{},error){
//	db := framework.Database{}
//	defer db.Close()
//	db.Select("id,nama,email,username,role")
//	db.From("admin")
//	db.StartGroup("AND")
//	db.StartGroup("OR")
//	db.Where("email", email)
//	db.WhereOr("username", email)
//	db.EndGroup()
//	db.Where("password", framework.Password(password))
//	db.Where("role",1)
//	db.EndGroup()
//	data, err := db.Row()
//	if err != nil {
//		return nil, err
//	}
//	return data, nil
//}
