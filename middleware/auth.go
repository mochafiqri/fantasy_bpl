package middleware

import (
	"fantasy_bpl/lib"
	"fmt"
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"fantasy_bpl/model"
)

func AuthUser(status ...uint8) gin.HandlerFunc {
	return func(c *gin.Context) {

		var tokenString string
		session := sessions.Default(c)
		v := session.Get(framework.Config("sessionName"))

		if v == nil {
			if status[0] == 255{
				return
			}
			notfound(session, c)
			c.Abort()
			return
		} else {
			tokenString = v.(string)
		}
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}

			return []byte(framework.Config("jwtKeyApi")), nil
		})
		if token == nil || err != nil {
			fmt.Println(err)
			notfound(session, c)
			c.Abort()
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)

		if !ok || !token.Valid {
			notfound(session, c)
			c.Abort()
			return
		} else {
			c.Set("jwt", claims)
		}

	}
}
func AuthAdmin(c *gin.Context) {
	var tokenString string
	session := sessions.Default(c)
	v := session.Get(framework.Config("sessionName"))

	if  v==nil{
		notfound(session,c)
		c.Abort()
		return
	}else{
		tokenString = v.(string)
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(framework.Config("jwtKeyApiAdmin")), nil
	})
	if token == nil || err != nil {
		fmt.Println(err)
		notfound(session,c)
		c.Abort()
		return
	}


	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok || !token.Valid {
		notfound(session,c)
		c.Abort()
		return
	}else{
		c.Set("jwt", claims)
	}

}


func AuthAPI(c *gin.Context) {
	var tokenString string
	tokenString = c.GetHeader("token")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(framework.Config("jwtKeyApi")), nil
	})
	if token == nil || err != nil {
		notfoundApi(c)
		c.Abort()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok || !token.Valid {
		notfoundApi(c)
		c.Abort()
		return
	}
	d, err := model.UserById(claims["id"].(string),"id_user",false)
	if err != nil || len(d) == 0 {
		fmt.Println(err)
		notfoundApi(c)
		c.Abort()
		return
	}
	c.Set("jwt", claims)

}

func notfound(session sessions.Session,c *gin.Context)  {
	session.Delete(framework.Config("jwtName"))
	session.Save()
	c.Redirect(http.StatusFound,"/login")
}

func notfoundApi(c *gin.Context) {
	lib.JSON(c, http.StatusForbidden, "Invalid Token", gin.H{})
}

