module fantasy_bpl

go 1.13

require (
	github.com/bandros/framework v1.1.6
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
