package router

import (
	"fantasy_bpl/controller"
	"fantasy_bpl/middleware"
	"github.com/gin-gonic/gin"
)

func user (r *gin.Engine) {
	rUser := r.Group("/user/api")

	rUser.POST("/klasemen-limit",controller.KlasemenLimit)
	rUser.POST("/klasemen-limit/byWeek",controller.KlasemenByWeekLimit)
	rUser.POST("/register",controller.UserRegister)

	rUser.POST("/klasemen",controller.Klasemen)
	//rUser.POST("/home/data",controller.Home)

	rUser.Use(middleware.AuthUser(1))
	rUser.POST("/my-team",controller.MyTeam)
	rUser.POST("/player/listByPosisi",controller.PlayerListByPosisi)
	rUser.POST("/player/transfer",controller.TransferPlayer)
	rUser.POST("/player/listAll",controller.PlayerAll)

	rUser.POST("/match_week/point",controller.PointByMatchWeek)




}
