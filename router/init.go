package router

import (
	"fantasy_bpl/controller"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Init(r *gin.Engine){
	r.Static("/asset", "./asset")
	r.Static("/public", "./public")
	r.LoadHTMLGlob("./pages/**/*")

	r.NoRoute(error404)
	r.NoMethod(error404)
	r.GET("/check_service", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"code": "200",
			"msg":  "Service running",
			"data": gin.H{},
		})
	})
	r.GET("/login",controller.LoginPage)
	r.POST("/login",controller.Login)
	r.GET("/logout", controller.Logout)
	admin(r)
	user(r)
	pageAdmin(r)
	pageUser(r)
}

func error404(c *gin.Context) {
	c.HTML(http.StatusOK,"template/404",gin.H{
		"title" : "Halaman Tidak Ditemukan",
	})
}


