package router

import (
	"fantasy_bpl/controller"
	"fantasy_bpl/middleware"
	"github.com/gin-gonic/gin"
)

func pageUser (r *gin.Engine) {

	r.GET("/", controller.PageMyTeam2)

	r.GET("/home",middleware.AuthUser(255), controller.Home)
	r.GET("/klasemen", controller.PageKlasemen)

	r.Use(middleware.AuthUser(1))
	r.GET("/my-team", controller.PageMyTeam2)
	r.GET("/point", controller.PagePoint)


}

