package router

import (
	"fantasy_bpl/controller"
	"fantasy_bpl/middleware"
	"github.com/gin-gonic/gin"
)

func pageAdmin (r *gin.Engine) {
	rAdmin := r.Group("/fantasyadmin")


	rAdmin.Use(middleware.AuthAdmin)
	rAdmin.GET("/", controller.Dashboard)


}
