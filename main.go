package main

import (
	"fantasy_bpl/router"
	"github.com/bandros/framework"
)

func main(){
	fw := framework.Init{}
	fw.Get()
	r := fw.Begin
	router.Init(r)
	fw.Run()
}
