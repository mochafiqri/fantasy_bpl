package lib

import (
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"time"
)

func GenerateTokenAdmin(admin map[string]interface{}) (string, error) {
	expHour, _ := strconv.Atoi(framework.Config("jwtExp"))
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	claims := sign.Claims.(jwt.MapClaims)
	claims["jti"] = framework.Password(admin["id_user"].(string) + string(time.Now().Unix()))
	claims["id"] = admin["id_user"]
	claims["email"] = admin["email"]
	claims["username"] = admin["username"]
	claims["role"] = admin["role"]
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(expHour)).Unix()
	return sign.SignedString([]byte(framework.Config("jwtKeyApiAdmin")))
}
func GenerateTokenUser(user map[string]interface{}) (string, error) {
	//expHour, _ := strconv.Atoi(framework.Config("jwtExp"))
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	claims := sign.Claims.(jwt.MapClaims)
	claims["jti"] = framework.Password(user["id_user"].(string) + string(time.Now().Unix()))
	claims["id"] = user["id_user"]
	claims["username"] = user["username"]
	claims["email"] = user["email"]
	claims["role"] = user["role"]
	claims["iat"] = time.Now().Unix()
	//claims["exp"] = time.Now().Add(time.Hour * time.Duration(expHour)).Unix()
	return sign.SignedString([]byte(framework.Config("jwtKeyApi")))
}
